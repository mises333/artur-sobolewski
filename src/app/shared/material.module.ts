import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
    MatSidenavModule, MatToolbarModule, MatIconModule,
    MatListModule, MatButtonModule,
} from '@angular/material';

@NgModule({
    imports: [
        CommonModule,
        MatListModule,
        MatButtonModule,
        NgbModule,
    ],
    entryComponents: [],
    exports: [CommonModule,
        MatSidenavModule, MatToolbarModule, MatIconModule,
        MatListModule, NgbModule,
    ],
    providers: []
})
export class MaterialModule { }