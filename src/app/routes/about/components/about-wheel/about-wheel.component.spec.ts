import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutWheelComponent } from './about-wheel.component';

describe('AboutWheelComponent', () => {
  let component: AboutWheelComponent;
  let fixture: ComponentFixture<AboutWheelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutWheelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutWheelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
