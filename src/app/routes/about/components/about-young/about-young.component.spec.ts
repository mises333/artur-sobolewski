import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutYoungComponent } from './about-young.component';

describe('AboutYoungComponent', () => {
  let component: AboutYoungComponent;
  let fixture: ComponentFixture<AboutYoungComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutYoungComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutYoungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
