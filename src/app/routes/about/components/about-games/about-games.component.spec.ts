import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutGamesComponent } from './about-games.component';

describe('AboutGamesComponent', () => {
  let component: AboutGamesComponent;
  let fixture: ComponentFixture<AboutGamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutGamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutGamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
