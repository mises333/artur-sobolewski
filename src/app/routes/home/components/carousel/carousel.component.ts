import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  videos = [];

  constructor(private carConfig: NgbCarouselConfig) { }

  ngOnInit() {
    this.carConfig.interval = 9000
    this.carConfig.pauseOnHover = false
    this.videos.push('https://firebasestorage.googleapis.com/v0/b/artur-sobolewski.appspot.com/o/videos%2Fnature.mp4?alt=media&token=a3c6da4a-96f8-4f85-a344-3c6a41c0e5fc')
    this.videos.push('https://firebasestorage.googleapis.com/v0/b/artur-sobolewski.appspot.com/o/videos%2FSpace.mp4?alt=media&token=205f7fbb-3e20-4f9e-ad36-51c35f426cb9')
    this.videos.push('https://firebasestorage.googleapis.com/v0/b/artur-sobolewski.appspot.com/o/videos%2Flandscape.mp4?alt=media&token=19e062b3-c9b1-4f55-a586-e3b4d47b2b13')
  }

}
