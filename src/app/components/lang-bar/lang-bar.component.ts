import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-lang-bar',
  templateUrl: './lang-bar.component.html',
  styleUrls: ['./lang-bar.component.scss']
})
export class LangBarComponent implements OnInit {

  constructor(private translate: TranslateService) { }

  ngOnInit() { }

  changeLang(lang: string) {
    this.translate.use(lang)
  }

}
