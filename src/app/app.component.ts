import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as $ from 'jquery';
import { CookieService } from 'ngx-cookie-service';
import { Router, NavigationEnd } from '@angular/router';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Artur-Sobolewski';

  // routerUrl: Observable<string>;
  // route: string[];

  constructor(private translate: TranslateService,
    private cookie: CookieService,
    private router: Router) { }

  ngOnInit(): void {
    this.translate.setDefaultLang('en')
    this.setLangByBrowser()
    this.windowPositionTop()
  }

  setLangByBrowser(): void {
    if (!this.cookie.check('user_lang')) {
      let browserLang = this.translate.getBrowserLang()
      this.translate.use(browserLang.match(/en|no|pl/) ? browserLang : "en/app")
      this.clickLangButton(browserLang)
    }
  }

  clickLangButton(lang: string) {
    $('document').ready(function () {
      switch (lang) {
        case "en":
          $('#en').trigger('click')
          break
        case "no":
          $('#no').trigger('click')
          break
        case "pl":
          $('#pl').trigger('click')
          break
        default:
      }
    })
  }

  // After navigate between routes set window position top.
  windowPositionTop() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return
      }
      // this.routerUrl = Observable.create(observer => observer.next(this.router.url))
      // this.routerUrl.subscribe(data => this.route = data.split('/'))
      window.scrollTo(0, 0)
    })
  }
}
