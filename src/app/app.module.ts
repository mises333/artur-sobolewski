import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './shared/material.module';
import { CookieService } from 'ngx-cookie-service';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';

import { HomeComponent } from './routes/home/home.component';
import { CarouselComponent } from './routes/home/components/carousel/carousel.component';
import { AboutComponent } from './routes/about/about.component';
import { ContactComponent } from './routes/contact/contact.component';
import { LangBarComponent } from './components/lang-bar/lang-bar.component';
import { HomeAboutComponent } from './routes/home/components/home-about/home-about.component';
import { PortfolioRowComponent } from './routes/home/components/portfolio-row/portfolio-row.component';
import { ContactRowComponent } from './routes/home/components/contact-row/contact-row.component';
import { HomeInterestsComponent } from './routes/home/components/home-interests/home-interests.component';
import { AboutYoungComponent } from './routes/about/components/about-young/about-young.component';
import { AboutWheelComponent } from './routes/about/components/about-wheel/about-wheel.component';
import { AboutGamesComponent } from './routes/about/components/about-games/about-games.component';
import { AboutWorkComponent } from './routes/about/components/about-work/about-work.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    LangBarComponent,
    CarouselComponent,
    HomeAboutComponent,
    PortfolioRowComponent,
    ContactRowComponent,
    HomeInterestsComponent,
    AboutYoungComponent,
    AboutWheelComponent,
    AboutGamesComponent,
    AboutWorkComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ],
  providers: [ CookieService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
